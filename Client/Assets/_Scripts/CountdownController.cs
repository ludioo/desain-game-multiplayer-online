﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownController : MonoBehaviour
{
    public int countdownTime;
    public Text countdownDisplay;
    public GameObject panelLose;
    public SceneController sceneController;

    public IEnumerator CountdownToStart()
    {
        while(countdownTime > 0)
        {
            countdownDisplay.text = countdownTime.ToString();
            yield return new WaitForSecondsRealtime(1f);
            countdownTime--;
        }

        countdownDisplay.text = "GO!";
        yield return new WaitForSecondsRealtime(1f);
        countdownDisplay.gameObject.SetActive(false);
        Time.timeScale = 1;
        StartCoroutine(CountdownToStartMatch());
    }
    // Start is called before the first frame update
    public int countdownTimeMatch;
    public Text countdownDisplayMatch;

    public IEnumerator CountdownToStartMatch()
    {
        while (countdownTimeMatch > 0)
        {
            countdownDisplayMatch.text = countdownTimeMatch.ToString();
            yield return new WaitForSeconds(1f);
            countdownTimeMatch--;
        }

        countdownDisplayMatch.text = "0";
        yield return new WaitForSeconds(1f);
        panelLose.SetActive(true);
        StartCoroutine(sceneController.BackToHome());
    }
}
