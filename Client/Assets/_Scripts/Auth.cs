﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Auth : MonoBehaviour
{
    public static PlayGamesPlatform platform;
    public Button test;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("TES");
        test.onClick.AddListener(Button);
    }
    void Button()
    {
        Debug.Log("NNN");
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;

        platform = PlayGamesPlatform.Activate();
        Social.localUser.Authenticate(success =>
        {
            if (success)
            {
                Debug.Log("Login Success!");
            }
            else
            {
                Debug.Log("Login Failed");
            }
        });
    }
}