﻿using UnityEngine;

public class ClickToMove : MonoBehaviour, IClickable
{
    public GameObject arrow;
    public GameObject player;
    private Navigator playerNavigator;
	void Start ()
	{
	    playerNavigator = player.GetComponent<Navigator>();
	}

    public void OnClick(RaycastHit hit)
    {
        playerNavigator.NavigateTo(hit.point);
        var arrowClone = (GameObject)Instantiate(arrow, hit.point, Quaternion.Euler(90,0,0));
        Destroy(arrowClone, 1);
		Network.Move(player.transform.position, hit.point);
    }
}
