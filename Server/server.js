var express = require('express') //menyimpan modul express kedalam variable
var moment = require('moment') //menyimpan modul moment yang berfungsi untuk mengambil data
var fs = require('fs') //menyimpan modul fs / filesystem yang berfungsi untuk mengakses file system
var app = express()
var http = require('http').Server(app); //menyimpann modul http yang digunakan untuk mengakses websocket yaitu http
var port = process.env.PORT || 3000; // menggunakan port 3000
var io = require('socket.io')(http); //menyimpan modul socket.io
var shortId = require('shortid'); //menyimpan modul shortid untuk membuat id Unik

app.use(express.static(__dirname))

var players = []; //Variable jumlah player

var playerSpeed = 3; //Variable kecepatan player

io.on('connection', function (socket) {

    var thisPlayerId = shortId.generate(); //menyimpan id player
    var time = moment().format("DD-MM-YYYY HH:mm:ss"); //mengakses momment dengan format tanggal dan jam
    var ip = socket.request.connection.remoteAddress.split(':ffff:')[1]; //Get the clients IP adress on socket.io

    //Menampilkan log menuju log.txt
    fs.appendFile('log.txt', `${time} - player with id ${thisPlayerId} and ip ${ip} is connected <br/>`, function (err) {
        if (err) {
            console.log(err)
        }
    })

    //Variable player yang menyimpan id dan posisi
    var player = {
        id: thisPlayerId,
        destination: {
            x: 0,
            y: 0
        },
        lastPosition: {
            x: 0,
            y: 0
        },
        lastMoveTime: 0
    };
    players[thisPlayerId] = player;

    //Jika player sudah lebih dari 1, maka memerintah informasi dari server ke client bahwa game telah dimulai
    if (Object.keys(players).length > 1) {
        io.sockets.emit("start");
        fs.appendFile('log.txt', `${time} - Game Started <br/>`, function (err) {
            if (err) {
                console.log(err)
            }
        })
    }

    //membuat parameter register untuk membuat unik id
    socket.emit('register', {
        id: thisPlayerId
    });

    //membuat dan membroadcast kepada seluruh client tentang parameter spwan
    socket.broadcast.emit('spawn', {
        id: thisPlayerId
    });

    //membuat dan membroadcast kepada seluruh client tentang parameter requestPostion
    socket.broadcast.emit('requestPosition');

    //jika player sudah memiliki id yang unik maka server akan diperintahkan untuk menspwan player sesuai player idnya.
    for (var playerId in players) {
        if (playerId == thisPlayerId)
            continue;
        socket.emit('spawn', players[playerId]);
    };

    //ketika salah satu player telah mencapai finish maka finsih akan di broadcastkan kepada semua client kecuali player tersebut.
    socket.on("finish", function (data) {
        socket.broadcast.emit("finish", {
            id: thisPlayerId
        });
    });

    //membuat parameter move yang digunakan untuk mengirim posisi dari server menuju client
    socket.on('move', function (data) {
        data.id = thisPlayerId;

        fs.appendFile('log.txt', `${time} - player with id ${thisPlayerId} moved ${JSON.stringify(data)}  <br/>`, function (err) {
            if (err) {
                console.log(err)
            }
        })

        player.destination.x = data.d.x;
        player.destination.y = data.d.y;

        var elapsedTime = Date.now() - player.lastMoveTime;

        var travelDistanceLimit = elapsedTime * playerSpeed / 1000;

        var requestedDistanceTraveled = lineDistance(player.lastPosition, data.c);

        player.lastMoveTime = Date.now();

        player.lastPosition = data.c;

        delete data.c;

        data.x = data.d.x;
        data.y = data.d.y;

        delete data.d;

        socket.broadcast.emit('move', data);
    });

    //membuat parameter updateposition untuk membroadcast update posisi dari sever menuju client
    socket.on('updatePosition', function (data) {
        data.id = thisPlayerId;
        socket.broadcast.emit('updatePosition', data);
    });

    //membuat parameter disconected, ketika salah satu player disconected maka object player dalam game akan dihapus dan log.txt akan dibersihkan.
    socket.on('disconnect', function () {
        console.log('client disconected');

        fs.appendFile('log.txt', `${time} - player with id ${thisPlayerId} disconnected <br/>`, function (err) {
            if (err) {
                console.log(err)
            }
        })
        delete players[thisPlayerId];
        if (Object.keys(players).length < 1) {
            fs.truncate('log.txt', 0, function (err) {
                if (err) {
                    console.log(err)
                }
            })
        }
        socket.broadcast.emit('disconnected', {
            id: thisPlayerId
        });
    });
});

function lineDistance(vectorA, vectorB) {
    var xs = 0;
    var ys = 0;

    xs = vectorB.x - vectorA.x;
    xs = xs * xs;

    ys = vectorB.y - vectorA.y;
    ys = ys * ys;

    return Math.sqrt(xs + ys);
}

var server = http.listen(port, () => {
    console.log(`server is running on port ${port}`)
})